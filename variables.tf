variable "region" {
  default     = "ap-southeast-2"
  description = "AWS Sydney Region"
}

variable "vpc-cidr-block" {
  default     = "10.0.0.0/16"
  description = "AWS Sydney Region cidr block"
}

variable "subnet-cidr-block" {
  default     = "10.0.10.0/24"
  description = "AWS Sydney Region Subnet cidr block"
}

variable "availability-zone-2a" {
  default     = "ap-southeast-2a"
  description = "Sydney Availability Zone 1"
}

variable "environment_prefix" {
  default     = "prod"
}

variable "my_ip" {
  default     = "150.107.175.166/32"
}

variable "instance_type" {
  default     = "t2.micro"
}

variable "my_public_key" {
  default     = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQCf08mHTcliZxIgwJttUaK+J7/JEXRrYvcQNYjWN5uMz5WNLZlu59Uv99U8bdPm059GKvFF193WUw/BAHjZH+Kozau4n6BPBFUMQqRDmlebL21eWpBDSczNAWw9Hw9oU2MU/swvaVFx0dvjT9u4m216HdfMbO3qt9927pLMf042uM6dAk6BI1Gwd0Qa7tp7A+fZ0E/pGs5pNnSR0CMMCuq6UaxBzVk1brEWZ1CfIjoALL4jVCHRlqct8U2DN646kL4lne2OIKxHLTI4kbp+dvgVolXgnD/NSUMMYqUDl9j8SzEzWjeLVa7pzYLM3lXrs9a4oBQHU1kS5BIF8vGb4OtWtKrWt2bWG6SSuJGdBpeoUwDKERGdvzle2JCkUx9/i1AbORUqLwls+Umr0IKhjnxc1QJhhKm5+YCa2gkfs5ODH7MAC5LfzhZ6Vr68jEJepnNhWmX9ikwVCMYQtnWvBDVVBZZVw2G7jiE91dzmFn+sVY5vT527fVItz3RGw/NzODM= sstuart@C02Z35D8LVDR"
}