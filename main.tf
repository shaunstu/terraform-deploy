provider "aws" {
  region        = var.region
}

resource "aws_vpc" "splunk-app-vpc" {
  cidr_block = var.vpc-cidr-block
  tags = {
    Name = "${var.environment_prefix}-vpc"
  }
}

resource "aws_subnet" "splunk-app-subnet-2a" {
  vpc_id             = aws_vpc.splunk-app-vpc.id
  cidr_block         = var.subnet-cidr-block
  availability_zone  = var.availability-zone-2a

  tags = {
    Name = "${var.environment_prefix}-subnet-2a"
  }
}

resource "aws_route_table" "splunk-app-route-table" {
  vpc_id = aws_vpc.splunk-app-vpc.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.splunk-app-igw.id

  }

  tags = {
    Name  : "${var.environment_prefix}-rtb"
  }

}

resource "aws_internet_gateway" "splunk-app-igw" {
  vpc_id = aws_vpc.splunk-app-vpc.id

  tags = {
    Name  : "${var.environment_prefix}-igw"
  }

}

resource "aws_route_table_association" "splunk-app-rt" {
  route_table_id = aws_route_table.splunk-app-route-table.id
  subnet_id       = aws_subnet.splunk-app-subnet-2a.id
}

resource "aws_security_group" "splunk-app-sg" {
  name = "splunk-app-sg"
  vpc_id = aws_vpc.splunk-app-vpc.id
  ingress {
    from_port = 22
    protocol  = "tcp"
    to_port   = 22
    cidr_blocks = [var.my_ip]
  }

  ingress {
    from_port = 8080
    protocol  = "tcp"
    to_port   = 8080
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port = 0
    protocol  = "-1"
    to_port   = 0
    cidr_blocks = ["0.0.0.0/0"]
    prefix_list_ids = []
    description = "Allow All Traffic Out"
  }
  tags = {
    Name = "${var.environment_prefix}-sg"
  }

}

data "aws_ami" "latest-amazon-linux-image" {
  owners = ["amazon"]
  most_recent = true
  filter {
    name   = "name"
    values = ["amzn2-ami-hvm-*-x86_64-gp2"]
  }
  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

}

output "aws_ami_id" {
  value = data.aws_ami.latest-amazon-linux-image.id
}

resource "aws_instance" "splunk-app-server" {
  ami = data.aws_ami.latest-amazon-linux-image.id
  instance_type = var.instance_type
  vpc_security_group_ids = [aws_security_group.splunk-app-sg.id]
  subnet_id = aws_subnet.splunk-app-subnet-2a.id
  availability_zone = var.availability-zone-2a
  associate_public_ip_address = true
  key_name = "shaunstuart"

  user_data = file("entry-script.sh")

  tags = {
    Name = "${var.environment_prefix}-server"
  }

}

output "ec2_public_ip" {
  value = aws_instance.splunk-app-server.public_ip
}

